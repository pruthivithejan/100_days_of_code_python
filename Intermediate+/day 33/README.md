# Day 32

Today we're learning about APIs (Application Programming Interface)

We can use the 'requests' module for calling API endpoints

```python
import requests

response = requests.get(url="http://api.open-notify.org/iss-now.json")
response.raise_for_status()

data = response.json()["iss_position"]
longitude = data["longitude"]
latitude = data["latitude"]

iss_position = (longitude, latitude)
print(iss_position)
```

- We build a [Kanye Qoute](./Kanye%20Qoute/) generator

- Lastly we build a [ISS Overhead](./ISS%20Overhead/) email sender
