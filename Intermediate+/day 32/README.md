# Day 32

Today we're going to send emails with python

### Using the smtplib module

```python
import smtplib

my_email = "pruthivithejan.code@gmail.com"
password = "xxxxxxxxxxxxxxxx"

with smtplib.SMTP("smtp.gmail.com") as connection:
    connection.starttls()
    connection.login(user=my_email , password=password)
    connection.sendmail(from_addr=my_email, to_addrs="pruthivithejan@outlook.com", msg="Hello")

```

### Using the datetime module

```python
import datetime as dt

now = dt.datetime.now()
print(now)


date_of_birth = dt.datetime(year=2002, month=6, day=28, hour=4)
print(date_of_birth)
```

We build two projects at the end

- [Monday Motivational Quotes](./quotes.py)
- [Birthday Wisher](./Birthday%20Wisher/)
