studentScores = {
 "Ron" : 78,
 "Harry" : 81,
 "Hermione" : 99,
 "Draco" : 74,
 "Neville" : 62
}

for student in studentScores:
    score = studentScores[student]
    if score > 90 :
        studentScores[student] = "Outstanding" 
    elif score > 80 :
        studentScores[student] = "Exceed Expectations"
    elif score > 70 :
        studentScores[student] = "Acceptable"
    else:
        studentScores[student] = "Fail"
        
print(studentScores)