travelLog = [
    {
      "Country": "United States",
      "TimesVisited" : 1,
      "CitiesVisited" : ["Detroit", "Miami"]
    },
    {
      "Country": "India",
      "TimesVisited" : 1,
      "CitiesVisited" : ["Delhi"]
    }
]

def addNewCountry (countryVisited, timesVisited, citiesVisited):
    newCountry = {}
    newCountry["Country"] = countryVisited
    newCountry["TimesVisited"] = timesVisited
    newCountry["CitiesVisited"] = citiesVisited
    travelLog.append(newCountry)



addNewCountry("Russia", 2, ["Moscow", "Saint Petersburg"])
print (travelLog)