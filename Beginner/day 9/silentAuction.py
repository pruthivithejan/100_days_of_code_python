logo = '''
                         ___________
                         \         /
                          )_______(
                          |"""""""|_.-._,.---------.,_.-._
                          |       | | |               | | ''-.
                          |       |_| |_             _| |_..-'
                          |_______| '-' `'---------'` '-'
                          )"""""""(
                         /_________\\
                       .-------------.
                      /_______________\\
'''

print(logo)
import os

def cls():
    os.system('cls' if os.name=='nt' else 'clear')



def countingBid(bids):
    highestBid = 0
    for key in bids:
          bidAmount = bids[key]
          if bidAmount > highestBid:
            highestBid = bidAmount
            highestBider = key
    
    print(f"\n The winner is {highestBider} and the bid is ${highestBid}. \n" + logo)


loop = True
bidLog= {}
while loop:
  Name = input("Type your name: \n")
  Bid = int(input("Type your bid : \n $"))
  bidLog[Name] = Bid

  countDecider = input(" Type 'yes' to add another bid or else type 'no': \n")
  if countDecider == 'no' :
    countingBid(bidLog)
    loop = False
  else:
    cls()


