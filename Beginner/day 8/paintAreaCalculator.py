import math

def paintAreaCalculator ( Width, Height):
    coverage = 5;
    areaOfWall = Width * Height
    paintCans = areaOfWall / coverage
    print(f"You'll need {math.ceil(paintCans)} cans of paint.")


wallHeight = int(input("Height of the wall :"))
wallWidth = int(input("Width of the wall :"))
paintAreaCalculator(wallHeight, wallWidth)