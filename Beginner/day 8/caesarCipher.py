logo = """           
 ,adPPYba, ,adPPYYba,  ,adPPYba, ,adPPYba, ,adPPYYba, 8b,dPPYba,  
a8"     "" ""     `Y8 a8P_____88 I8[    "" ""     `Y8 88P'   "Y8  
8b         ,adPPPPP88 8PP"""""""  `"Y8ba,  ,adPPPPP88 88          
"8a,   ,aa 88,    ,88 "8b,   ,aa aa    ]8I 88,    ,88 88          
 `"Ybbd8"' `"8bbdP"Y8  `"Ybbd8"' `"YbbdP"' `"8bbdP"Y8 88   
            88             88                                 
           ""             88                                 
                          88                                 
 ,adPPYba, 88 8b,dPPYba,  88,dPPYba,   ,adPPYba, 8b,dPPYba,  
a8"     "" 88 88P'    "8a 88P'    "8a a8P_____88 88P'   "Y8  
8b         88 88       d8 88       88 8PP""""""" 88          
"8a,   ,aa 88 88b,   ,a8" 88       88 "8b,   ,aa 88          
 `"Ybbd8"' 88 88`YbbdP"'  88       88  `"Ybbd8"' 88          
              88                                             
              88           
"""

alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
print(logo)



"""
# This is the way to go around your nose

def encrypt( plainText, shiftAmount):
    cipherText = ""
    for letter in plainText:
        position = alphabet.index(letter)
        newPosition = position + shiftAmount
        newLetter = alphabet[newPosition]
        cipherText += newLetter
    print(f"The encoded text is {cipherText}")

def decrypt( cipherText, shiftAmount):
    plainText = "" 
    for letter in cipherText:
        position = alphabet.index(letter)
        newPosition = position - shiftAmount
        newLetter = alphabet[newPosition]
        plainText += newLetter
    print(f"The decoded text is {plainText}")

if direction == 'encode':
 encrypt(text, shift)
elif direction == 'decode' :
 decrypt(text, shift)
"""

""" 
# This is the easier way to do it.
def caesar(startText, shiftAmount, cipherDirection):
    endText = ""
    for letter in startText:
        position = alphabet.index(letter)
        if cipherDirection == 'encode':
            newPosition = position + shiftAmount
        elif cipherDirection == 'decode':
            newPosition = position - shiftAmount
        newLetter = alphabet[newPosition]
        endText += newLetter
    print(f"The text is '{endText}'.")

caesar(text, shift, direction)
"""

"""
# if you're even smarter, this will be the function
def caesar(startText, shiftAmount, cipherDirection):
    endText = ""
    if cipherDirection == 'decode':
            shiftAmount *= -1
    for letter in startText:
        position = alphabet.index(letter)
        newPosition = position + shiftAmount
        newLetter = alphabet[newPosition]
        endText += newLetter
    print(f"The text is '{endText}'.")

caesar(text, shift, direction)
"""

# some more functions added to the original code



def caesar(startText, shiftAmount, cipherDirection):
    endText = ""
    if cipherDirection == 'decode':
     shiftAmount *= -1
    for char in startText:
     if char in alphabet : 
         position = alphabet.index(char)
         newPosition = position + shiftAmount
         newLetter = alphabet[newPosition]
         endText += newLetter
     else:      
         endText += char  
    print(f"The text is '{endText}'.")
 
        

loop = True
while loop:
  direction = input("Type 'encode' to encrypt, type 'decode' to decrypt: \n")
  text = input("Type your message: \n").lower()
  shift = int(input("Type the shift number : \n"))
  shift = shift % 26
  caesar(text, shift, direction)

  countDecider = input(" Type 'yes' to run again or else type 'no'")
  if countDecider == 'no' :
    loop = False
    print("Good Bye!")
