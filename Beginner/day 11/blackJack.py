import random
import os

def cls():
    os.system('cls' if os.name=='nt' else 'clear')

logo = """

.------.            _     _            _    _            _    
|A_  _ |.          | |   | |          | |  (_)          | |   
|( \/ ).-----.     | |__ | | __ _  ___| | ___  __ _  ___| | __
| \  /|K /\  |     | '_ \| |/ _` |/ __| |/ / |/ _` |/ __| |/ /
|  \/ | /  \ |     | |_) | | (_| | (__|   <| | (_| | (__|   < 
`-----| \  / |     |_.__/|_|\__,_|\___|_|\_\ |\__,_|\___|_|\_\\
      |  \/ K|                            _/ |                
      `------'                           |__/           
"""


# ===RULES ===
# The deck is infinite
# 2 playes, King,Queen and Jack = 10 points
# If your cards add up to more than 21 you're busted.
# Ace can either be 1 points or 11 points to your liking


def deal_card():
    """Deal a random card out of the deck"""
    cards = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]
    random_card = random.choice(cards)
    return random_card

def calculate_score(cards):
    if sum(cards) == 21 and len(cards) == 2:
        return 0
    
    if 11 in cards and sum(cards) > 21:
        cards.remove(11)
        cards.append(1)

    return sum(cards)

def compare(users_score, computers_score):
    if users_score == computers_score:
        return "Draw 🤝"
    elif computers_score == 0:
        return "Lose, opponent has BlackJack 😒"
    elif users_score == 0:
        return "Win with a BlackJack 😁"
    elif users_score > 21 :
        return "You went over. You lose 🥲"
    elif computers_score > 21:
        return "Opponent went over. You win 😆"
    elif users_score > computers_score:
        return "You win 🤩"
    else: 
        return "You lose 😥"

# def BlackJack():

print(logo)

computer_cards = []
player_cards = []
isGameOver = False

for _ in range(2):
    player_cards.append(deal_card())
    computer_cards.append(deal_card())

while not isGameOver :

    players_score = calculate_score(player_cards)
    computers_score = calculate_score(computer_cards)
    print(f" Your Cards: {player_cards}, Current Score: {players_score}\n")
    print(f" Computer's First Card: {computer_cards[0]}")

    if players_score == 0 or computers_score == 0 or players_score > 21 :
        isGameOver = True
    else :
        continue_playing = input("Type 'y' to get another card, type 'n' to pass: \n")
        if continue_playing == "y" :
            player_cards.append(deal_card())
        elif continue_playing == "n":
            isGameOver = True

while computers_score != 0 and computers_score < 17:
    computer_cards.append(deal_card())
    computers_score = calculate_score(computer_cards)

print(f" Your final hand : {player_cards}, Final score: {players_score}\n")
print(f" Computer's final hand: {computer_cards}, Final score: {computers_score}\n")

print(compare(players_score, computers_score))

    
# BlackJack()


# anotherRound = input("Do you want to play a game of BlackJack? (y/n):")
# while anotherRound == "y":
    
   

     
