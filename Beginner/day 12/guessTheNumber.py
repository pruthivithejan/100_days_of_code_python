
# TODO: A function that outputs a random number between 1 and 100.
# TODO: A function that imputs user numbers and outputs 'Too High' or 'Too Low'.
# TODO: A function lets you choose between level 'easy' or 'hard' and return the number of attempts
# TODO: A function that repeats till the number of attempts and run the second function and outputs the result

import random

Intro = """

.d88b                           88888 8              8b  8                 8               
8P www 8   8 .d88b d88b d88b      8   8d8b. .d88b    8Ybm8 8   8 8d8b.d8b. 88b. .d88b 8d8b 
8b  d8 8b d8 8.dP' `Yb. `Yb.      8   8P Y8 8.dP'    8  "8 8b d8 8P Y8P Y8 8  8 8.dP' 8P   
`Y88P' `Y8P8 `Y88P Y88P Y88P      8   8   8 `Y88P    8   8 `Y8P8 8   8   8 88P' `Y88P 8    
                                                                                                                                                                                              
"""
print(Intro)

Number = random.randint(1, 100)	

Level = input("I'm thinking of a number between 1 and 100.\n Choose a difficulty level. Type 'easy' or 'hard' :")

if Level == 'easy' :
   Attempts = 10
elif Level == 'hard' :
   Attempts = 5

def highLow():
    userNumber = int(input("Make a guess:"))
    if userNumber > Number :
      print("Too High")
    elif userNumber < Number :
       print("Too Low")
    elif userNumber == Number :
       print("You got it")
       return 0
       

while Attempts > 0 :
   print(f"You have {Attempts} attempts remaining to guess the number.")
   if highLow() == 0:
      break
   Attempts -= 1