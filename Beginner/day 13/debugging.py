############DEBUGGING#####################

# 1.Describe Problem
# A Logical Error : for loops olny checks if the value is less than the value of the last value of the range.
# Changing the range from (1,21) will solve this
def my_function():
  for i in range(1, 20):
    if i == 20:
      print("You got it")
my_function()

# 2.Reproduce the Bug
# A Logical Error : Arrays start with 0
# Changing the range from (0,5) will solve this
from random import randint
dice_imgs = ["❶", "❷", "❸", "❹", "❺", "❻"]
dice_num = randint(1, 6)
print(dice_imgs[dice_num])

# 3.Play Computer
# A Logical Error : None of the if statements include 1994 in it's range
# Fix : elif year >= 1994:
year = int(input("What's your year of birth?"))
if year > 1980 and year < 1994:
  print("You are a millenial.")
elif year > 1994:
  print("You are a Gen Z.")

# 4.Fix the Errors
# Syntax Error : Expected indented block
# Fix: indent the print statement
# Runtime Error : Inputing a string and checking for a integer
# Fix : Cast the input string to a integer
age = int(input("How old are you?"))
if age > 18:
 print(f"You can drive at age {age}.")

# 5.Print is Your Friend
# Logical Error : = is a assigning operator and == is equal operator
# Fix : word_per_page = int(input("Number of words per page: "))
pages = 0
word_per_page = 0
pages = int(input("Number of pages: "))
word_per_page == int(input("Number of words per page: "))
total_words = pages * word_per_page
print(total_words)

# 6.Use a Debugger
# Logical Error : b_list is outside the loop and will only include the last result
# Fix : indent b_list inside the loop
def mutate(a_list):
  b_list = []
  for item in a_list:
    new_item = item * 2
  b_list.append(new_item)
  print(b_list)

mutate([1,2,3,5,8,13])

# 7. Take a break

# 8. Ask a Developer 

# 9. Run Often

# 10. Use GPT for debugger