logo = """

 _____________________
|  _________________  |
| | Pruthivi's   0. | |  .----------------.  .----------------.  .----------------.  .----------------. 
| |_________________| | | .--------------. || .--------------. || .--------------. || .--------------. |
|  ___ ___ ___   ___  | | |     ______   | || |      __      | || |   _____      | || |     ______   | |
| | 7 | 8 | 9 | | + | | | |   .' ___  |  | || |     /  \     | || |  |_   _|     | || |   .' ___  |  | |
| |___|___|___| |___| | | |  / .'   \_|  | || |    / /\ \    | || |    | |       | || |  / .'   \_|  | |
| | 4 | 5 | 6 | | - | | | |  | |         | || |   / ____ \   | || |    | |   _   | || |  | |         | |
| |___|___|___| |___| | | |  \ `.___.'\  | || | _/ /    \ \_ | || |   _| |__/ |  | || |  \ `.___.'\  | |
| | 1 | 2 | 3 | | x | | | |   `._____.'  | || ||____|  |____|| || |  |________|  | || |   `._____.'  | |
| |___|___|___| |___| | | |              | || |              | || |              | || |              | |
| | . | 0 | = | | / | | | '--------------' || '--------------' || '--------------' || '--------------' |
| |___|___|___| |___| |  '----------------'  '----------------'  '----------------'  '----------------' 
|_____________________|
"""
print(logo, "\n")


def Multiplication(num1, num2):
    return num1 * num2


def Divition(num1, num2):
    return num1 / num2


def Addition(num1, num2):
    return num1 + num2


def Subtraction(num1, num2):
    return num1 - num2


operations = {
    "+": Addition,
    "-": Subtraction,
    "*": Multiplication,
    "/": Divition
}


def calculator():
    number1 = float(input("First Number: \n"))
    for symbol in operations:
        print(symbol)

    Continue = True

    while Continue:
        operation_symbol = input("Pick an operation: \n")
        number2 = float(input("Next Number: \n"))
        calculation = operations[operation_symbol]
        answer = calculation(number1, number2)

        print(f"{number1} {operation_symbol} {number2} = {answer}")

        repeater = input(
            f"Type 'y' to continue calculating with {answer}, or type 'n' to start a new calculation, or type 'e' to exit:\n")

        if repeater == "e":
            Continue = False
        elif repeater == "y":
            number1 = answer
        elif repeater == "n":
            calculator()


calculator()
