
# Check if the year is leap or not and returning True or False according to that.
def is_leap(year):
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                return True
            else:
                return False
        else:
            return True
    else:
        return False

# This function will give you the number of months in a year


def days_in_month(year, month):
    month_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if month > 12 or month < 1:
        return "Invalid month"
    else:
        if is_leap(year) and month == 2:
            return 29
        return month_days[month - 1]

    # This is also valid but longer.
    """checked_year = is_leap(year)
    if checked_year == True:
        month_days[1] = "29"
        num_of_days = month_days[month-1]
    else:
        num_of_days = month_days[month-1]
    return num_of_days"""


# User inputs and outputs
Year = int(input("Enter Year:\n"))
Month = int(input("Enter Month:\n"))
days = days_in_month(Year, Month)
print("\nNumber of days in the month: ", days)
