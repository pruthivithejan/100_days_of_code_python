from turtle import Turtle, Screen
import random

screen = Screen()
screen.setup(width=500, height=400)
screen.title("Welcome to the turtle race!")
user_bet = screen.textinput(title="Make your bet",
                            prompt="Which turtle will win the race? Enter a color (red, orange, yellow, green, blue, purple): ")
colors = ["red", "orange", "yellow", "green", "blue", "purple"]
turtles_racing = []
y = -60
is_racing = False

for turtle_index in range(6):
    new_turtle = Turtle(shape="turtle")
    new_turtle.color(colors[turtle_index])
    new_turtle.penup()
    new_turtle.goto(x=-230, y=y)
    y += 30
    turtles_racing.append(new_turtle)


if user_bet:
    is_racing = True


while is_racing:
    for turtle in turtles_racing:
        if turtle.xcor() > 230:
            is_racing = False
            won_color = turtle.pencolor()
            if won_color == user_bet:
                print(f"You've won! The {won_color} turtle is the winner!")
                screen.write(
                    f"You've won! The {won_color} turtle is the winner!")
            else:
                print(f"You've lost! The {won_color} turtle is the winner!")
        turtle_distance = random.randint(0, 10)
        turtle.forward(turtle_distance)


screen.exitonclick()
