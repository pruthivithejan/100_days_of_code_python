# Day 19

## Higher Order Functions

- Higher order functions in Python are special kinds of functions that allow you to pass other functions as arguments or return them from your own functions. Here are some simple examples:

```python
def add(a):
  def double_and_add(x):
    return x + (a * 2)
  return double_and_add

# Calling add() function above
doubled = add(5)(3) # returns 17
```

In this example, we define two functions - add() which takes one argument "a" and another function called double_and_add which takes any number "x". Inside `double_an

Explainde by [HuggingChat](https://huggingface.co/chat/)

- Higher-order functions are functions that can take other functions as parameters or return a function as output. In Python, functions are treated as first-class objects, which means they can be passed as arguments, returned as values, and stored in data structures like lists or dictionaries.

Here's a simple example to help you understand higher-order functions:

```python
def greet(func):
    return func("Hello, I'm a higher-order function!")

def shout(text):
    return text.upper()

def whisper(text):
    return text.lower()

# Using the greet function with different functions as argument
print(greet(shout))
print(greet(whisper))

```

in this example, greet is a higher-order function because it takes another function (either shout or whisper) as an argument. When you run this code, it will output:

```
HELLO, I'M A HIGHER-ORDER FUNCTION!
hello, i'm a higher-order function!

```

Explained by [Phind](https://www.phind.com/)

---

- Also we make a Etch a Sketch app with event listeners in turtle graphics

[Etch A Sketch ✒️](./EtchASketch.py)

- At the end we built a Turtle Racing game

## [Turtle Racing Game 🐢](./TurtleRace.py)
