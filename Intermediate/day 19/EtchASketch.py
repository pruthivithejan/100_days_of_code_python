from turtle import Turtle, Screen
brizzy = Turtle()
screen = Screen()


def move_fowards():
    brizzy.forward(10)


def move_backards():
    brizzy.backward(10)


def turn_left():
    brizzy.left(10)


def turn_right():
    brizzy.right(10)


def clear():
    brizzy.clear()
    brizzy.penup()
    brizzy.home()
    brizzy.pendown()


screen.listen()
screen.onkey(move_fowards, "w")
screen.onkey(move_backards, "s")
screen.onkey(turn_left, "a")
screen.onkey(turn_right, "d")
screen.onkey(clear, "c")
screen.exitonclick()
