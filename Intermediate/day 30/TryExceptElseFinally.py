# ---- File Not Found

# try:
#     file = open("file.txt")
# except FileNotFoundError:
#     file = open("file.txt", "w")
#     file.write("Pruthivi Thejan \n")
# else:
#     print(file.read())
# finally:
#     file.close()
#     print("File closed.")

# ---- Raise Your Own Error

# def divide_numbers(a, b):
#     if b == 0:
#         raise ValueError("Cannot divide by zero.")
#     return a / b


# try:
#     result = divide_numbers(10, 0)
# except ValueError as ve:
#     print(f"Error: {ve}")
# else:
#     print(f"Result: {result}")


# ---- Index Error

# fruits = ["Apple", "Pear", "Orange"]


# def make_pie(index):

#     try:
#         fruit = fruits[index]

#     except IndexError:
#         print("fruit pie ready!")
#     else:
#         print(fruit + " pie ready!")
#     finally:
#         print("Eat the pie")


# make_pie(2)


# ---- Key Error in Dictionary

# facebook_posts = [{"Likes": 21, "Comments": 2, }, {
#     "Likes": 41, "Comments": 9, "Shares": 3}, {"Likes": 31, "Comments": 5, "Shares": 1}, {"Likes": 2, "Comments": 1, }, {"Comments": 55, "Shares": 19}, {"Comments": 9, "Shares": 11},]

# total_likes = 0

# for post in facebook_posts:
#     try:
#         total_likes = total_likes + post["Likes"]
#     except KeyError:
#         pass

# print(total_likes)
