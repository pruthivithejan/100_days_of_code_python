# Errors & Exceptions

We talked about 4 main errors in Python

- File Not Found Error

```python
try:
    with open('nonexistent_file.txt', 'r') as file:
        content = file.read()
except FileNotFoundError:
    print("File not found error: The specified file does not exist.")
else:
    print("File read successfully.")
finally:
    print("This block always executes, regardless of whether there was an exception.")

```

- Key Error (Dictionary)

```python
my_dict = {'key1': 'value1', 'key2': 'value2'}
try:
    value = my_dict['nonexistent_key']
except KeyError:
    print("Key error: The specified key does not exist in the dictionary.")
else:
    print("Key found successfully.")
finally:
    print("This block always executes, regardless of whether there was an exception.")

```

- Index Error

```python
my_list = [1, 2, 3]
try:
    element = my_list[5]
except IndexError:
    print("Index error: Index is out of range for the list.")
else:
    print("Index accessed successfully.")
finally:
    print("This block always executes, regardless of whether there was an exception.")

```

- Type Error

```python
try:
    result = '2' + 2
except TypeError:
    print("Type error: Cannot concatenate str and int.")
else:
    print("Operation successful.")
finally:
    print("This block always executes, regardless of whether there was an exception.")

```

---

- We can have multiple "except" statements
- Use "raise" to make errors

```python
def divide_numbers(a, b):
    if b == 0:
        raise ValueError("Cannot divide by zero.")
    return a / b

try:
    result = divide_numbers(10, 0)
except ValueError as ve:
    print(f"Error: {ve}")
else:
    print(f"Result: {result}")

```

In this example, the divide_numbers function raises a ValueError with the message "Cannot divide by zero" if the second argument (b) is zero. The try block attempts to call this function with divide_numbers(10, 0), which triggers the ValueError. The except block catches the exception and prints an error message. If the division is possible (e.g., divide_numbers(10, 2)), the else block is executed and prints the result.

- We also updated the NATO alphabet project with try exceptions
  [NATO Alphabet](./NATO%20Alpahabet%20-%20Error%20Handling/)

- We also updated the Password management project with try exceptions and more
  [Password Manager](./Password%20Manager%20-%20Updated/)

---

## Working with JSON

```python
import json

# Specify the file name
json_file_name = 'example.json'

# Load existing JSON data from the file
try:
    with open(json_file_name, 'r') as json_file:
        existing_data = json.load(json_file)
except FileNotFoundError:
    existing_data = {}

# Display the existing data
print("Existing Data:")
print(existing_data)

# Update the existing data or add new key-value pairs
existing_data['age'] = 26
existing_data['new_key'] = 'new_value'

# Display the updated data
print("\nUpdated Data:")
print(existing_data)

# Write the updated data back to the JSON file
with open(json_file_name, 'w') as json_file:
    json.dump(existing_data, json_file, indent=2)

print(f"The data has been updated and written to {json_file_name}.")

```
