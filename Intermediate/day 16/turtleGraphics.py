from turtle import Turtle, Screen

# Creating a new Object from a blueprint
brizzy = Turtle()
myScreen = Screen()

# Printing Attributes from the Object using Object.Attribute
print(myScreen.canvheight)

# Calling Methods from the Object
brizzy.shape("turtle")
brizzy.color("orange")
brizzy.forward(100)
myScreen.exitonclick()
