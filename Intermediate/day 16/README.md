# Object Oriented Programming (OOP)

- The concept that we used to write programs this far is called Procedural programming.

- This concept is harder to use when the program gets bigger.

### So today we are going to rewrite the code we written yesterday using OOP concepts.

So I used GPT-4 to generate me a explanation for OOP considering that I'm 12 years old.

# What is OOP ?

Object-oriented programming (OOP) is a way of writing computer programs that is based on the idea of objects. Objects are like little machines that can do things and store information. For example, imagine you have a toy box. The toy box is like an object. It can store toys (information) and you can open and close it (do things). In OOP, programmers create objects that can store information and do things, just like the toy box. This makes it easier to organize and reuse code when writing programs.

Does that make sense? 😊

# What are Attributes and Methods ?

In object-oriented programming (OOP), objects have attributes and methods. Attributes are like characteristics or properties of an object. For example, if we have a toy car object, its attributes could be its color, size, and type of wheels. Methods are like actions that an object can do. For example, our toy car object could have methods like drive, stop, and honk.

Does that help? 😊

# What is a class and what is a object ?

A class is like a blueprint or template for creating objects. It defines the properties and methods that an object of that class will have. An object is an instance of a class that has its own state, behavior, and identity.

For example, imagine we have a class called “ToyCar” that defines the properties and methods for toy cars. We can use this class to create toy car objects. Each toy car object will have its own color, size, and type of wheels (properties) and can drive, stop, and honk (methods).

Does that help? 😊

Note :

```
car = CarBlueprint()
```

as shown above car is the _object_ that we create form the blueprint of _CarBluePrint()_ which is a _class_

- To demonstrate this example we are goint to use the turtle graphics library built into Python.

[Turtel Graphics Example 🐢](turtleGraphics.py)

- For fun I've tried an example from the Turtle graphics documentation

[Turtel Star Example ⭐](turtleStar.py)

- For the next example I had to install some external packages. I installed the package using,

```python
pip install PrettyTable
```

- At last we rewrote the coffe machine code in OOP

[Coffee Machine ☕](./coffeMachineOOP/main.py)
