from tkinter import *
import random
import pandas as pd

BG_COLOR = "#B1DDC6"
FONT_NAME = 'Nohemi'

current_card = {}
to_learn = {}

try:
    data = pd.read_csv("data/french_words.csv")
except FileNotFoundError:
    original_data = pd.read_csv("data/french_words.csv")
    to_learn = original_data.to_dict(orient="records")
else:
    to_learn = data.to_dict(orient="records")


def next_card():
    global current_card, flip_timer
    window.after_cancel(flip_timer)
    current_card = random.choice(to_learn)
    canvas.itemconfig(card_title, text="French", fill="black")
    canvas.itemconfig(card_word, text=current_card["French"], fill="black")
    canvas.itemconfig(image, image=card_front)
    flip_timer = window.after(3000, func=flip_card)


def flip_card():
    canvas.itemconfig(card_title, text="English", fill="white")
    canvas.itemconfig(card_word, text=current_card["English"], fill="white")
    canvas.itemconfig(image, image=card_back)


def right():
    to_learn.remove(current_card)
    data = pd.DataFrame(to_learn)
    data.to_csv("data/words_to_learn.csv", index=False)

    next_card()


window = Tk()
window.title("Flash Card 🗂️")
window.config(padx=50, pady=50, bg=BG_COLOR)

flip_timer = window.after(3000, func=flip_card)

canvas = Canvas(width=800, height=526, bg=BG_COLOR, highlightthickness=0)
card_front = PhotoImage(file="images/card_front.png")
card_back = PhotoImage(file="images/card_back.png")
image = canvas.create_image(400, 263, image=card_front)
canvas.grid(column=0, row=0, columnspan=2)

title = "French"
card_title = canvas.create_text(
    400, 150, text=title, font=(FONT_NAME, 40, "italic"))

word = "Flash Card"
card_word = canvas.create_text(
    400, 263, text=word, font=(FONT_NAME, 24, "bold"),)

wrong_button_image = PhotoImage(file="images/wrong.png")
wrong_button = Button(image=wrong_button_image,
                      command=next_card, highlightthickness=0)
wrong_button.grid(column=0, row=1)

correct_button_image = PhotoImage(file="images/right.png")
correct_button = Button(image=correct_button_image,
                        command=right, highlightthickness=0)
correct_button.grid(column=1, row=1)

next_card()


window.mainloop()
