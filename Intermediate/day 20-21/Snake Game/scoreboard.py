from turtle import Turtle

ALIGNMENT = "center"
FONT = ("Courier", 16, "normal")
SINGLE_SCORE = 1


class ScoreBoard(Turtle):
    def __init__(self):
        super().__init__()
        self.score = 0
        self.penup()
        self.goto(0, 270)
        self.color("white")
        self.hideturtle()
        self.update_scoreboard()

    def update_scoreboard(self):
        self.write(f"Score = {self.score}", font=(
            FONT), align=ALIGNMENT)

    def game_over(self):
        self.goto(0, 0)
        self.write(f"GAME OVER", font=(
            FONT), align=ALIGNMENT)

    def scored(self):
        self.score += SINGLE_SCORE
        self.clear()
        self.update_scoreboard()
