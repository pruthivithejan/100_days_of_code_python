# Snake Game 🐍

- In the days of 20 and 21 we're going to make the worrld famous Snake Game

[Snake Game 🐍](/Snake%20Game/)

![Snake Game Build with Python](./Snake%20Game/demo.gif)

## Class Inheritance

Inheritance is like being part of a family. Just like how you inherit some traits from your parents, in programming, one class can inherit traits from another class.

For example, let's say you have a class called "Animal" that has some basic traits like "has fur" and "can make sounds". Now, you want to create a more specific class called "Dog". Instead of starting from scratch and defining all the traits again, you can simply make "Dog" inherit the traits from "Animal".

So, "Dog" will automatically have the traits of "Animal", plus any additional traits you add specifically for "Dog", like "can bark" or "has a tail".

Here's some code to illustrate the concept:

```python
class Animal:
    def __init__(self):
        self.has_fur = True
        self.can_make_sounds = True

class Dog(Animal):
    def __init__(self):
        super().__init__() # Call the parent class constructor
        self.can_bark = True
        self.has_tail = True

```

In this example, "Dog" is inheriting from "Animal" using the parentheses in its definition. Then, in the constructor for "Dog", it calls the constructor for "Animal" using super().**init**(), which sets the "has_fur" and "can_make_sounds" traits.

Finally, it adds its own traits of "can_bark" and "has_tail".

So, now you can create an instance of "Dog" and it will automatically have all the traits of "Animal" as well:

```python
my_dog = Dog()
print(my_dog.has_fur) # True
print(my_dog.can_make_sounds) # True
print(my_dog.can_bark) # True
print(my_dog.has_tail) # True

```

Explained by [ChatGPT](https://chat.openai.com/)

Here is an another example :

```python
class Dog:
    def __init__(self):
        self.temperament = "loyal"

    def bark(self):
        print("Woof, woof!")

class Labrador(Dog):
    def __init__(self):
        super().__init__()
        self.is_a_good_boy = True

    def bark(self):
        super().bark()
        print("Greetings, good sir. How do you do?")

```

If I print the bark() from Labrador this is the result,

```python
sparky = Labrador()
sparky.bark()
```

Output :

```
Woof, woof!
Greetings, good sir. How do you do?
```

---

## Slicing

Imagine that you have a sandwich with different layers. Slicing is like cutting the sandwich to get a specific layer you want to eat.

For example, if you have a list of fruits that goes like this:

```python

fruits = ["apple", "banana", "orange", "kiwi", "pineapple"]

fruits_slice = fruits[1:3]
print(fruits_slice)
```

The number before the colon (1 in this case) is the starting point of the slice, and the number after the colon (3 in this case) is the ending point of the slice.

So, the slicing would start at the second fruit (because Python starts counting at 0), and end at the third fruit (not including it).

The result would be a new list with the sliced fruits:

```
["banana", "orange"]
```

If you use fruits[::-1] to slice the fruits list, it will return a reversed copy of the original list.

The third parameter in a slice, which is not specified in this case, determines the step size. If it is left empty, it defaults to 1. A negative step size (-1 in this case) means that the slice will be taken in reverse order.

So, fruits[::-1] means "give me all the elements in the fruits list, but in reverse order".

For example, using the same fruits list as before, fruits[::-1] would return:

```
["pineapple", "kiwi", "orange", "banana", "apple"]
```

This means that the last element of the original list becomes the first element in the sliced list, the second-to-last element becomes the second element, and so on.

Explained by [ChatGPT](https://chat.openai.com/)
