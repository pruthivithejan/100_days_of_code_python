# Day 24

## File Handling

More info can be found here
: https://www.w3schools.com/python/python_file_handling.asp

---

File management in Python refers to the ability to perform various operations on files and directories. Python provides a rich set of built-in modules and functions to handle file-related tasks efficiently. The primary modules used for file management in Python are os and shutil.

Creating and Opening Files: Python allows you to create new files using the open() function. You can specify the file name, mode (read, write, append), and encoding. For example, to open a file named "example.txt" in write mode:

```python
file = open("example.txt", "w")
```

Writing to Files: Once a file is opened in write mode, you can write data to it using the write() method. For example:

```python
file.write("Hello, World!")
```

Closing Files: After writing or reading from a file, it's important to close it using the close() method to release system resources. For example:
python

```python
file.close()
```

Reading from Files: To read data from a file, you can open it in read mode and use the read() or readlines() methods. The read() method returns the entire file content as a string, while readlines() returns a list of lines. For example:

```python
file = open("example.txt", "r")
content = file.read()
print(content)
file.close()
```

Checking File Existence: The os module provides functions to perform operations related to the operating system, including file management. You can check if a file exists using the os.path.exists() function. For example:
python

```python
import os

if os.path.exists("example.txt"):
   print("File exists")
else:
   print("File does not exist")
```

Copying and Moving Files: The shutil module offers convenient functions for copying and moving files and directories. The shutil.copy() function can be used to copy files, while shutil.move() can be used to move or rename files. For example:
python

```python
import shutil

# Copy file
shutil.copy("source.txt", "destination.txt")

# Move file
shutil.move("old_name.txt", "new_name.txt")
```

Deleting Files: You can delete files using the os.remove() function. For example:

```python
import os

os.remove("file_to_delete.txt")
```

These are just a few of the basic file management operations in Python. The os and shutil modules provide many more functions for advanced file handling, such as creating directories, traversing directory trees, changing file permissions, and more.

---

```python
with open("example.txt", "r") as file:
    content = file.read()
    # Perform operations with the file

# Outside the 'with' block, the file is automatically closed

```

Using the with statement for file handling is considered a best practice because it simplifies the code and eliminates the need for manually closing the file using the _close()_ method.

---

We've updated the snake game to have a High score value

[Snake Game with High Score 🐍](./Snake%20Game%20-%20High%20Score/)

---

In Python, paths are used to specify the location of files and directories on a file system. There are two types of paths: absolute paths and relative paths.

1. Absolute Paths: An absolute path provides the complete location of a file or directory from the root of the file system. It includes all the necessary information to locate the file or directory, such as the drive letter (in Windows) and the full directory structure. Absolute paths remain constant regardless of the current working directory. Here's an example of an absolute path:

Windows: C:\Users\Username\Documents\example.txt
Linux/Mac: /home/username/Documents/example.txt
To work with absolute paths in Python, you can use the os.path module, which provides functions for manipulating paths, such as joining paths, checking existence, and extracting components.

2. Relative Paths: A relative path specifies the location of a file or directory relative to the current working directory. It does not start from the root of the file system but instead assumes a starting point based on the current location. Relative paths are typically shorter and more flexible because they can be used across different systems and directory structures.

To specify a relative path, you use one or more path components separated by a directory separator character (/ or \, depending on the operating system). Here are some examples of relative paths:

example.txt: Refers to a file in the current working directory.
folder/subfolder/file.txt: Refers to a file located inside the folder directory, which is inside the current working directory.
When working with relative paths, it's important to be aware of the current working directory, which is the directory from which your Python script or program is being executed. You can obtain the current working directory using the os.getcwd() function from the os module.

To manipulate relative paths in Python, you can use the os.path module to join paths, resolve parent directories (..), check existence, and perform other operations.

Here's an example of using relative paths to open a file:

```python
import os

# Get the current working directory
current_dir = os.getcwd()

# Specify the relative path to the file
relative_path = "folder/example.txt"

# Join the current directory and the relative path
file_path = os.path.join(current_dir, relative_path)

# Open the file using the absolute path
with open(file_path, "r") as file:
    content = file.read()
    # Perform operations with the file

```

In the example above, the os.path.join() function is used to combine the current working directory with the relative path to create an absolute path. The resulting file_path can then be used to open the file using the with statement, as shown earlier.

Explained by [ChatGPT](https://chat.openai.com/)

---

I've coded a program to replace names of invitations in seconds

[Mail Merge 📨📬](./Mail%20Merge/main.py)
