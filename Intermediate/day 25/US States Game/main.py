import turtle
import pandas

IMAGE = "blank_states_img.gif"

screen = turtle.Screen()
screen.title("U.S. States Game")
screen.addshape(IMAGE)

turtle.shape(IMAGE)

data = pandas.read_csv("50_states.csv")
# pen = turtle.Turtle()

# ============================== MY SOLUTION ===============================
# def writeState(x, y, state):
#     pen.penup()
#     pen.goto(x, y)
#     pen.pendown()
#     pen.write(state, align="center", font=("Arial", 16, "normal"))


# game_is_on = True

# while game_is_on:
#     answer = screen.textinput(title="Guess the State",
#                               prompt="What's another state's name ?")
#     xCorr = data.x[data.state == f"{answer.capitalize()}"]
#     yCorr = data.y[data.state == f"{answer.capitalize()}"]
#     state = data.state[data.state == f"{answer.capitalize()}"]
#     x = int(xCorr.iloc[0])
#     y = int(yCorr.iloc[0])
#     writeState(x, y, state)

# ============================== COURSE SOLUTION ===============================
guessed_states = []
all_states = data.state.to_list()

while len(guessed_states) < 50:

    answer_state = screen.textinput(title=f"{len(guessed_states)}/50 States Correct",
                                    prompt="What's another state's name ?").title()

    if answer_state == "Exit":
        missing_states = []
        for state in all_states:
            if state not in guessed_states:
                missing_states.append(state)
        new_data = pandas.DataFrame(missing_states)
        new_data.to_csv("states_to_learn.csv")
        break
    if answer_state in all_states:
        guessed_states.append(answer_state)
        t = turtle.Turtle()
        t.hideturtle()
        t.penup()
        state_data = data[data.state == answer_state]
        t.goto(int(state_data.x), int(state_data.y))
        t.write(answer_state.capitalize())
