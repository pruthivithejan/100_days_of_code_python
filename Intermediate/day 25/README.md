# Day 25

## Reading CSV (Comma Seperated Values) and Data

Working with CSV files in Python is quite straightforward. You can use the built-in csv module to read from and write to CSV files. Here's a step-by-step guide on how to work with CSV files in Python:

1. Import the csv module:

```python
import csv
```

2. Reading data from a CSV file:

To read data from a CSV file, you'll need to open the file in "read" mode using the open() function and then create a csv.reader object. You can iterate over the rows of the CSV file using a for loop.

```python
with open('data.csv', 'r') as file:
    csv_reader = csv.reader(file)
    for row in csv_reader:
        # Each row represents a list of values
        print(row)

```

By default, the values in each row will be treated as strings. If you want to parse them as other types, you can convert them accordingly.

3. Writing data to a CSV file:

To write data to a CSV file, you'll need to open the file in "write" mode using the open() function and then create a csv.writer object. You can use the writerow() method of the csv.writer object to write rows to the CSV file.

```python
with open('data.csv', 'w') as file:
    csv_writer = csv.writer(file)
    csv_writer.writerow(['Name', 'Age', 'City'])
    csv_writer.writerow(['John Doe', 30, 'New York'])
    csv_writer.writerow(['Jane Smith', 25, 'London'])

```

In the example above, we first write the header row (containing column names) using writerow(), and then we write the data rows.

4. Specifying delimiters and quoting characters:

By default, the csv module uses commas (,) as the delimiter and double quotes (") as the quoting character. However, you can customize these settings by specifying the delimiter and quotechar parameters when creating the csv.reader or csv.writer objects.

```python
# Reading with a different delimiter
with open('data.csv', 'r') as file:
    csv_reader = csv.reader(file, delimiter=';')

# Writing with a different delimiter and quoting character
with open('data.csv', 'w') as file:
    csv_writer = csv.writer(file, delimiter=',', quotechar="'")

```

These are the basic steps for working with CSV files in Python using the csv module. You can explore more functionality and options provided by the csv module in the Python documentation: https://docs.python.org/3/library/csv.html

---

## Pandas Library 🐼

Working with the Pandas library in Python is a great way to manipulate and analyze data. As a beginner, here are some steps to get started with Pandas:

1. Install Pandas: Before you can use Pandas, make sure it is installed on your system. You can install it using pip, the Python package installer, by running the following command in your terminal or command prompt:

```python
pip install pandas
```

2. Import the library: Once Pandas is installed, you need to import it into your Python script or Jupyter Notebook:

```python
import pandas as pd
```

3. Load data into a DataFrame: The primary data structure in Pandas is the DataFrame. You can create a DataFrame by loading data from various sources, such as CSV files, Excel files, or databases. For example, to load data from a CSV file, you can use the read_csv() function:

```python
df = pd.read_csv('data.csv')
```

4. Explore the data: After loading the data into a DataFrame, you can start exploring it. Some useful methods to get an overview of the data include:

```python
df.head()       # Returns the first few rows of the DataFrame
df.tail()       # Returns the last few rows of the DataFrame
df.shape        # Returns the number of rows and columns in the DataFrame
df.info()       # Provides a summary of the DataFrame, including data types and missing values
df.describe()   # Generates descriptive statistics for numerical columns
```

5. Accessing and manipulating data: Pandas provides various ways to access and manipulate data within a DataFrame. Here are a few examples:

```python
df['column_name']             # Access a specific column by name
df[['col1', 'col2']]         # Access multiple columns by name
df.iloc[row_index]            # Access a specific row by index
df.loc[row_label]             # Access a specific row by label
df['new_column'] = values     # Create a new column with specified values
df.dropna()                   # Drop rows with missing values
df.groupby('column_name')     # Group data based on a column
```

6. Data cleaning and preprocessing: Often, the data you work with may require cleaning and preprocessing. Pandas offers numerous functions for these tasks, such as handling missing values, removing duplicates, transforming data types, and more. Some commonly used methods include:

```python
df.dropna()                    # Drop rows with missing values
df.fillna(value)               # Fill missing values with a specified value
df.duplicated()                # Check for and remove duplicates
df.astype({'column_name': type})  # Convert the data type of a column
df.replace(old_value, new_value)  # Replace specific values in the DataFrame
```

7. Data analysis and visualization: Pandas integrates well with other libraries, such as NumPy and Matplotlib, to perform advanced data analysis and visualization. You can apply mathematical operations, aggregate data, plot graphs, and more. Here's an example of plotting a histogram using Matplotlib:

```python
import matplotlib.pyplot as plt

df['column_name'].plot.hist(bins=10)
plt.show()

```

These steps should help you get started with Pandas as a beginner. Remember to refer to the Pandas documentation (https://pandas.pydata.org/docs/) for more detailed information on the library's capabilities and additional functionalities.

Explained by [ChatGPT](https://chat.openai.com/)

---

We used the Panda library for some basic data analysis

- [Weather Data](./WeatherData.py) 🌞

- [Squirrel Data](./SquirrelData.py) 🐿️

Finally, We built a quiz for guessing the states of U.S.

[U.S. State Game ](./US%20States%20Game/main.py)
