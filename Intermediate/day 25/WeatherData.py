# with open("weather_data.csv", "r") as data_file:
#     data = data_file.readlines()
#     print(data)

# ====================== Using CSV =======================
# import csv

# with open("weather_data.csv", "r") as data_file:
#     data = csv.reader(data_file)
#     temperatures = []
#     for row in data:
#         if row[1] != "temp":
#             temperatures.append(int(row[1]))
#     print(temperatures)

# ====================== Using Pandas =======================

import pandas

data = pandas.read_csv("weather_data.csv")

# data_dictionary = data.to_dict()

# temperatures = data["temp"].to_list()

# average_temperatures = sum(temperatures) / len(temperatures)

# average_temperatures = data["temp"].mean()

# maximum_temperatures = data["temp"].max()

# maximum_temperature_day = data.day[data.temp == data.temp.max()]
# print(maximum_temperature_day)

# monday = data[data.day == "Monday"]
# monday_temp = int(monday.temp)
# monday_temp_f = monday_temp * 9/5 + 32
# print(monday_temp_f)
