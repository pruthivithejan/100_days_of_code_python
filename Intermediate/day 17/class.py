# class User:
#     def __init__(self):
#         print("New user being created")
#     # you can use 'pass' to make this empty by not making a sytax error

# user_1 = User()
# user_1.name = "Johnny"
# user_1.id = "001"
# user_1.email = "johnny@proton.me"

# print(user_1.email)




# class FamilySubscription:
#     def __init__(self, members):
#         self.members = members

# my_family = FamilySubscription(4)
# print(my_family.members) # 4


class User:
    def __init__(self, user_id, name):
        self.user_id = user_id
        self.name = name
        self.followers = 0
        self.followings = 0
    
    def Follow(self, user):
        user.followers += 1
        self.followings += 1

user_1 = User("001", "Jordy")
user_2 = User("002", "Hardy")

user_1.Follow(user_2)
print(user_1.followers, user_1.followings)
print(user_2.followers, user_2.followings)
