# Day 17

- Today we are going to create our own classes

We can create a class by using,

```python
class MyClass:
  print("The code")
```

The classes uses the _PascalCase_ naming convention

### Creating Attributes

You can create attributes to the class using,

```python
MyClass.id = "001"
MyClass.value= "some value"
```

To stop repeating ourselves we can use a _Constructor_ to create the attributes

```python
class User:
    def __init__(self):
        print("New user being created")
    # you can use 'pass' to make this empty by not making a sytax error

user_1 = User()
user_1.name = "Johnny"
user_1.id = "001"
user_1.email = "johnny@proton.me"

print(user_1.email)
# New user being created
# johnny@proton.me
```

every time a Object is created the _**init**_ function is called

so you can create a attribute like this,

```python
class FamilySubscription:
    def __init__(self, members):
        self.members = members

my_family = FamilySubscription(4)
print(my_family.members) # 4

```

We can provide default values to the attributes inside the constructor. In that case that attribute will not be required

### Creating Methods

To create a method let's use the User example above to create a Follow Method to follow users

```python
class User:
    def __init__(self, user_id, name):
        self.user_id = user_id
        self.name = name
        self.followers = 0
        self.followings = 0

    def Follow(self, user):
        user.followers += 1
        self.followings += 1

user_1 = User("001", "Jordy")
user_2 = User("002", "Hardy")

user_1.Follow(user_2)
print(user_1.followers, user_1.followings)
print(user_2.followers, user_2.followings)
```

All the above code and more examples are in [here](./class.py).

- Next we are going to make a Quiz Project using the knowledge of classes

[Quiz Project 💡](./quizProject/main.py)
