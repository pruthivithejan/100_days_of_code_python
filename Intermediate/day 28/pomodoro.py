from tkinter import *
# ----------------------------- CONSTANTS -------------------------------#
BG_COLOR = "#fca5a5"
FG_COLOR = "#dc2626"
CHECK_COLOR = "#16a34a"
FONT_NAME = 'Nohemi'
WORK_MIN = 25
SHORT_BREAK = 5
LONG_BREAK = 20
CHECK = "✔️"
reps = 0
timer = None



# ----------------------------- TIMER RESET -------------------------------#

def reset_timer():
  window.after_cancel(timer)
  canvas.itemconfig(timer_text, text="00:00")
  title.config(text="Timer")
  check_mark.config(text="")
  global reps
  reps = 0




# ----------------------------- TIMER MECHANISM -------------------------------#\

def start_timer():
   global reps
   reps += 1
   work_sec = WORK_MIN * 60
   short_break_sec = SHORT_BREAK * 60
   long_break_sec = LONG_BREAK * 60

   if reps % 8 == 0:
    count_down(long_break_sec)
    title.config(text="Break", fg=CHECK_COLOR)
   elif reps % 2 == 0:
    count_down(short_break_sec)
    title.config(text="Break", fg=CHECK_COLOR)
   elif reps % 2 == 1:
    count_down(work_sec)
    title.config(text="Work", fg=FG_COLOR)
   
   
   

# ----------------------------- COUNTDOWN MECHANISM -------------------------------#

def count_down(count):
    minuites = count // 60
    seconds = count % 60
    if seconds < 10:
       seconds = f"0{seconds}"

    canvas.itemconfig(timer_text, text=f"{minuites}:{seconds}")
    if count > 0 :
     global timer
     timer = window.after(1000, count_down, count-1)
    else :
      start_timer()
      mark = ""
      for _ in range(reps//2):
        mark += CHECK
      check_mark.config(text=mark)




# ----------------------------- UI SETUP -------------------------------#

window = Tk()
window.title("Pomodoro Timer 🍅")
window.config(padx=20, pady=20, bg=BG_COLOR)
window.minsize(width=500, height=450)


title = Label(text="Timer", fg=FG_COLOR, bg=BG_COLOR , font=(FONT_NAME, 35, "bold"))
title.grid(column=1, row=1)

canvas = Canvas(width=300, height=300, bg=BG_COLOR, highlightthickness=0)
png = PhotoImage(file="tomato.png")
canvas.create_image(150,150, image=png)
timer_text = canvas.create_text(150,180, text="00:00", fill="white", font=(FONT_NAME, 35, "bold"))
canvas.grid(column=1, row=2)

start_button = Button(text="Start", fg=FG_COLOR, bg=BG_COLOR , font=(FONT_NAME, 15, "bold"), command=start_timer)
start_button.grid(column=0, row=3)

reset_button = Button(text="Reset", fg=FG_COLOR, bg=BG_COLOR , font=(FONT_NAME, 15, "normal"), command=reset_timer)
reset_button.grid(column=3, row=3)

check_mark = Label(fg=CHECK_COLOR, bg=BG_COLOR , font=(FONT_NAME, 18, "bold"))
check_mark.grid(column=1, row=3)

window.mainloop()