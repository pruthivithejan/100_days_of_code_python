import turtle as t
import random

pen = t.Turtle()
t.colormode(255)


def random_color():
    r = random.randint(0, 255)
    g = random.randint(0, 255)
    b = random.randint(0, 255)
    random_color = (r, g, b)
    return random_color


directions = [0, 90, 180, 270,]
speed = [2, 4, 6, 10, 15, 20]

pen.pensize(15)


for _ in range(300):
    pen.color(random_color())
    pen.speed(random.choice(speed))
    pen.forward(30)
    pen.setheading(random.choice(directions))

screen = t.Screen()
screen.exitonclick()
