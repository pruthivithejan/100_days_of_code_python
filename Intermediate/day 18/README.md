# Day 18

- At the start we used turtle graphics to draw a square for warming up

[Square with Turtle Graphics](./turtleChallange1.py)

---

# Importing

- We can import a module in many different ways

## Basic Import

```python
import turtle

brizzy = turtle.Turtle
```

## 'from' Import

```python
from turtle import Turtle

brizzy = Turtle()
squeezy = Turtle()
```

This will be useful if you make lots of Objects with the imported package.

## Import All

```python
from turtle import *

shape("turtle")
color("green")
Screen().exitonclick()
```

This is easy but will be confusing when you look at the code later.

## Aliasing Modules

```python
import turtle as tortoise

brizzy = tortoise.Turtle()
```

You can name the package whatever you want in this method.

---

- At next we used turtle graphics to draw a dashed line

[Dashed line with Turtle Graphics](./turtleChallange2.py)

- Next we draw a triangle, square, pentagon, hexagon, heptagon, octagon, nonagon and decagon starting from the same point.

[Polygons with Turtle Graphics](./turtlePolygons.py)

- Next we made a Random Walk with colors

[Random Walk with Turtle Graphics](./turtleRandomWalk.py)

- After that we made a Spirograph with turtle graphics

[Spirograph with Turtle Graphics](./turtleSpirograph.py)

---

Finally we draw a random dot paint with turtle graphics

[Color Dots inspired by Damien Hirst ](./Color%20Dots/)
