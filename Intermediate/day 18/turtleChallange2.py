from turtle import Turtle, Screen

pen = Turtle()

# Drawing a dashed line
for _ in range(31):
    pen.penup()
    pen.forward(5)
    pen.pendown()
    pen.forward(5)


screen = Screen()
screen.exitonclick()
