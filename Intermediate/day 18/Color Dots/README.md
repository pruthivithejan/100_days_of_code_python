# Color Dots inspired by Damien Hirst

In this module, I learned a package that will be so useful to me as a designer.

It's called colorgram.py. This package can extract colors from a image and make a color palette.

To install run,

```python
pip install colorgram.py
```

I have made a example that extract and return the colors of a given image below,

[Color Extractor 🌈](./colorPalette.py)

Getting a RGB color palette

```python
import colorgram

colors = colorgram.extract('dot-img.jpg', 30)
color_palette = []

for count in range(len(colors)):
    rgb = colors[count]
    color = rgb.rgb
    color_palette.append(color)

print(color_palette)
```

_Remember to run the code in the same relative path that the image and the code in included or you will get an error_

### _I've modified the code to extract the colors below the values of (224,224,224) which is named 'Gainsboro'._

### _This will give you a list of colors that are not near to white._

```python
import colorgram

colors = colorgram.extract('dot-img.jpg', 30)
color_palette = []

for count in range(len(colors)):
    color = colors[count].rgb
    color_palette.append(color)

rgb_colors = []

for i in range(len(color_palette)):
    r = color_palette[i].r
    g = color_palette[i].g
    b = color_palette[i].b
    if r < 224 and g < 224 and b < 224:
        new_color = (r, g, b)
        rgb_colors.append(new_color)

print(rgb_colors)
```

---

After extracting the colors we wrote the Color Dots program,

[Color Dots Program 🔴🔵🟢](./colorDots.py)

I've left some mistakes in the code and didn't watched the solution
