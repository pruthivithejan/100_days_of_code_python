import colorgram

colors = colorgram.extract('dot-img.jpg', 30)
color_palette = []

for count in range(len(colors)):
    color = colors[count].rgb
    color_palette.append(color)

rgb_colors = []

for i in range(len(color_palette)):
    r = color_palette[i].r
    g = color_palette[i].g
    b = color_palette[i].b
    if r < 224 and g < 224 and b < 224:
        new_color = (r, g, b)
        rgb_colors.append(new_color)

print(rgb_colors)

# useful_colors = [(214, 157, 85), (33, 105, 151), (153, 75, 52), (125, 168, 199), (209, 134, 163), (156, 60, 81), (22, 39, 54), (212, 85, 61), (176, 162, 47), (200, 85, 119),
#                  (135, 184, 150), (56, 119, 90), (25, 46, 37), (64, 46, 34), (87, 157, 100), (9, 99, 75), (34, 166, 189), (40, 60, 102), (179, 189, 213), (95, 126, 173), (68, 34, 44), (105, 42, 60)]
