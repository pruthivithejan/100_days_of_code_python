import turtle as t
import random

useful_colors = [(214, 157, 85), (33, 105, 151), (153, 75, 52), (125, 168, 199), (209, 134, 163), (156, 60, 81), (22, 39, 54), (212, 85, 61), (176, 162, 47), (200, 85, 119),
                 (135, 184, 150), (56, 119, 90), (25, 46, 37), (64, 46, 34), (87, 157, 100), (9, 99, 75), (34, 166, 189), (40, 60, 102), (179, 189, 213), (95, 126, 173), (68, 34, 44), (105, 42, 60)]

pen = t.Turtle()
t.colormode(255)


def random_color():
    color_number = random.randint(0, len(useful_colors)-1)
    random_color = (useful_colors[color_number])
    return random_color  # (r,g,b)


gap = 0

for _ in range(10):
    pen.penup()
    pen.setx(0)
    pen.pendown()
    for _ in range(10):
        pen.color(random_color())
        pen.dot(20)
        pen.penup()
        pen.forward(50)
        pen.pendown()
    gap += 50
    pen.penup()
    pen.sety(gap)
    pen.pendown()

screen = t.Screen()
screen.exitonclick()
