import turtle as t
import random

pen = t.Turtle()
t.colormode(255)


def random_color():
    r = random.randint(0, 255)
    g = random.randint(0, 255)
    b = random.randint(0, 255)
    color = (r, g, b)
    return color


pen.speed(11)


def spirograph(gap):
    for _ in range(360 // gap):
        pen.color(random_color())
        pen.circle(100)
        pen.setheading(pen.heading() + gap)


spirograph(5)


# +++ Variant 02 +++
# for _ in range(180):
#     pen.color(random_color())
#     pen.tilt(30)
#     pen.forward(2)
#     pen.left(170)
#     pen.circle(100)


screen = t.Screen()
screen.exitonclick()
