from turtle import Turtle, Screen
import random

pen = Turtle()

# Drawing a triangle, square, pentagon, hexagon, heptagon, octagon, nonagon and decagon


def polygon(num_of_legs, color):
    angle = 360/num_of_legs
    for _ in range(num_of_legs):
        pen.color(color)
        pen.forward(100)
        pen.right(angle)


color_palette = ["black", "red", "green", "yellow", "blue", "orange"]


for i in range(3, 11):
    color = random.choice(color_palette)
    polygon(i, color)

screen = Screen()
screen.exitonclick()
