MENU = {
    "espresso": {
        "ingredients": {
            "water": 50,
            "coffee": 18,
        },
        "cost": 1.5,
    },
    "latte": {
        "ingredients": {
            "water": 200,
            "milk": 150,
            "coffee": 24,
        },
        "cost": 2.5,
    },
    "cappuccino": {
        "ingredients": {
            "water": 250,
            "milk": 100,
            "coffee": 24,
        },
        "cost": 3.0,
    }
}

resources = {
    "water": 300,
    "milk": 200,
    "coffee": 100,
}

# money = 0

# def Exchange():
#  coinValue = {"penny": 0.01, "nickles": 0.05, "dime": 0.1, "quarters" : 0.25}
#  print("Please insert coins.")
#  quarters = input("How many quarters?:")
#  dimes = input("How many dimes?:")
#  nickles = input("How many nickles?:")
#  pennies = input("How many pennies?:")

# order = input("What would you like? (expresso/latte/cappuccino):").lower()

# if order == "report":
#  print(f" Water: {resources['water']}\n Milk: {resources['milk']}\n Coffee: {resources['coffee']}\n Money: ${money}")
# elif order == "expresso":
#  print("Please insert coins.")

profit = 0
isOn = True

def isSufficient(orderIngredients):
  for item in orderIngredients:
    if orderIngredients[item] >= resources[item]:
        print(f"Sorry there is not enough {item}.")
        return False
  return True  

def processCoins():
  print("Please insert coins.")
  total = int(input("How many quarters?:")) * 0.25
  total += int(input("How many dimes?:")) * 0.1
  total += int(input("How many nickles?:")) * 0.25
  total += int(input("How many pennies?:")) * 0.01
  return total

def transactionSucceeded(moneyReceived, drinkCost):
  if moneyReceived >= drinkCost:
    change = round(moneyReceived - drinkCost , 2)
    print(f"Here is ${change} in change.")
    global profit
    profit += drinkCost
    return True
  else: 
    print("Sorry that's not enough money. Money refunded.")
    return False

def makeCoffee(drinkName, orderIngredients):
  for item in orderIngredients:
    resources[item] -= orderIngredients[item]
  print(f"Here is your {drinkName} ☕")


while isOn:
  choice = input("What would you like? (expresso/latte/cappuccino):").lower()
  if choice == "off":
    isOn = False
  elif choice == "report":
    print(f" Water: {resources['water']}ml \n Milk: {resources['milk']}ml \n Coffee: {resources['coffee']}ml \n Money: ${profit}")
  else: 
    drink =  MENU[choice]
    if isSufficient(drink["ingredients"]):
      payment = processCoins()
      if transactionSucceeded(payment, drink["cost"]):
        makeCoffee(choice, drink["ingredients"])
