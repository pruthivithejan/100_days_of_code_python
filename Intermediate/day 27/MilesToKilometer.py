from tkinter import *

FONT = ("Nohemi", 16, "bold")

def miles_to_kilometers():
    miles = float(miles_input.get())
    km = miles * 1.609
    kilometer_result_label.config(text=f"{km}")

window = Tk()
window.title("Miles To Kilometers 🧮🏃")
window.config(padx=20, pady=20)
window.minsize(width=400, height=100)

miles_input = Entry(width=7, font=FONT)
miles_input.grid(column=1, row=0)

miles_label = Label(text="Miles", font=FONT)
miles_label.grid(column=2, row=0)

is_equal = Label(text="is equal to", font=FONT)
is_equal.grid(column=0, row=1)

kilometer_result_label = Label(text="0", font=FONT)
kilometer_result_label.grid(column=1, row=1)

kilomete_label = Label(text="Km", font=FONT)
kilomete_label.grid(column=2, row=1)

calcutate_button = Button(text="Calculate", font=FONT, command=miles_to_kilometers)
calcutate_button.grid(column=2, row=0)

window.mainloop()
