import random
import tkinter

FONT = ("Nohemi", 24, "bold")

window = tkinter.Tk()
window.title("Tkinter GUI")
window.minsize(width=500, height=300)


# Label
label = tkinter.Label(text="Pruthivi Thejan", font=FONT)
label.pack(pady=10)

# Button

info = ["Developer & Designer", "21",
        "from Sri Lanka", "Undergraduate at USJP", "Pruthivi Thejan"]


def onClick():
    i = random.randint(0, len(info) - 1)
    label.config(text=info[i])


button = tkinter.Button(text="Next", font=(
    "Nohemi", 16, "bold"), command=onClick)
button.pack()


window.mainloop()
