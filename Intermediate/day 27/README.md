# Day 27

## Python Function Arguments

In Python, function arguments are the values or variables that are passed to a function when it is called. Arguments allow you to provide input to a function, enabling it to perform operations or computations based on the given values. Python functions can accept different types of arguments, including positional arguments, keyword arguments, default arguments, and variable-length arguments.

- Positional Arguments: These are the arguments passed to a function in the order specified by the function's parameter list. The values are assigned to the parameters based on their respective positions. The number of arguments passed must match the number of parameters defined by the function.

Example:

```python
def greet(name, age):
    print(f"Hello, {name}! You are {age} years old.")

greet("Alice", 25)  # Output: Hello, Alice! You are 25 years old.
```

- Keyword Arguments: These arguments are passed to a function using the parameter name followed by the value, allowing you to specify the values for specific parameters regardless of their order.

Example:

```python
def greet(name, age):
    print(f"Hello, {name}! You are {age} years old.")

greet(age=25, name="Alice")  # Output: Hello, Alice! You are 25 years old.
```

- Default Arguments: These are parameters with predefined values. If an argument is not provided for a default parameter, the default value is used. Default arguments are defined by assigning a value to the parameter in the function declaration.

Example:

```python
def greet(name, age=30):
    print(f"Hello, {name}! You are {age} years old.")

greet("Alice")  # Output: Hello, Alice! You are 30 years old.
greet("Bob", 40)  # Output: Hello, Bob! You are 40 years old.
```

- Variable-Length Arguments: Python functions can accept a variable number of arguments using the *args or \*\*kwargs syntax. The *args parameter allows passing a variable number of positional arguments as a tuple, while the \*\*kwargs parameter allows passing a variable number of keyword arguments as a dictionary.

## Variable-Length Arguments

Here are examples of using variable-length arguments (`*args` and `**kwargs`) in Python:

1. Using `*args` (Variable-Length Positional Arguments):

```python
def calculate_sum(*numbers):
    total = sum(numbers)
    print(f"The sum is: {total}")

calculate_sum(1, 2, 3)  # Output: The sum is: 6
calculate_sum(4, 5, 6, 7, 8)  # Output: The sum is: 30
```

In this example, the `calculate_sum` function accepts a variable number of positional arguments using `*numbers`. The arguments passed to the function are treated as a tuple, allowing you to perform operations on them, such as calculating their sum.

2. Using `**kwargs` (Variable-Length Keyword Arguments):

```python
def print_person_info(**person):
    print("Person Information:")
    for key, value in person.items():
        print(f"{key}: {value}")

print_person_info(name="Alice", age=25, city="New York")
# Output:
# Person Information:
# name: Alice
# age: 25
# city: New York
```

In this example, the `print_person_info` function accepts a variable number of keyword arguments using `**person`. The arguments passed to the function are treated as a dictionary, allowing you to access and print the key-value pairs.

3. Combining `*args` and `**kwargs`:

```python
def process_data(*args, **kwargs):
    print("Positional Arguments:")
    for arg in args:
        print(arg)
    print("Keyword Arguments:")
    for key, value in kwargs.items():
        print(f"{key}: {value}")

process_data(1, 2, 3, name="Alice", age=25)
# Output:
# Positional Arguments:
# 1
# 2
# 3
# Keyword Arguments:
# name: Alice
# age: 25
```

In this example, the `process_data` function accepts both variable-length positional arguments (`*args`) and variable-length keyword arguments (`**kwargs`). It demonstrates how you can handle and process different types of arguments simultaneously.

Variable-length arguments are useful when you want to create functions that can handle a varying number of arguments, making your code more flexible and adaptable to different situations.

Explained by [ChatGPT](https://chat.openai.com/)

---

## Tkinter

Today we tried the Tkinter GUI package.

- [Tkinter Introduction 🤷‍♂️](./Tkinter.py)
- [Tkinter Demo 📲](./Tkinter-Demo.py)

In Python's Tkinter library, the `.pack()` and `.place()` methods are used to position and layout widgets within a window or container. They provide different approaches for organizing the placement of widgets on the screen.

1. `.pack()` Method:
   The `.pack()` method is a simple and convenient way to arrange widgets in a container using a stacking layout. When you use `.pack()`, widgets are automatically stacked vertically or horizontally, depending on the orientation you specify. It determines the size of each widget based on its content and available space.

Example:

```python
import tkinter as tk

root = tk.Tk()

label1 = tk.Label(root, text="Label 1")
label2 = tk.Label(root, text="Label 2")

label1.pack()  # Will be placed at the top
label2.pack()  # Will be placed below label1

root.mainloop()
```

In this example, the labels are stacked vertically using `.pack()`. The first label (`label1`) is placed at the top, and the second label (`label2`) is placed below it. The widgets expand to fill the width of the container.

2. `.place()` Method:
   The `.place()` method allows you to precisely position widgets by specifying their exact coordinates. You can determine the exact location (x and y coordinates) of the widget, as well as its width and height. This method gives you more control over the widget placement.

Example:

```python
import tkinter as tk

root = tk.Tk()

label1 = tk.Label(root, text="Label 1")
label2 = tk.Label(root, text="Label 2")

label1.place(x=10, y=10)  # Positioned at (10, 10)
label2.place(x=50, y=50)  # Positioned at (50, 50)

root.mainloop()
```

In this example, the labels are positioned using the `.place()` method. The first label (`label1`) is placed at coordinates (10, 10), and the second label (`label2`) is placed at coordinates (50, 50). You have precise control over the widget's position on the screen.

The choice between `.pack()` and `.place()` depends on the specific requirements of your GUI layout. Here are some considerations:

- `.pack()` is more suitable for simple layouts and quick prototyping. It automatically handles the widget stacking and resizing based on available space.
- `.place()` is useful when you need precise control over widget positioning. It allows you to specify exact coordinates and control the size of the widgets.
- For complex layouts, you might need to combine different geometry managers (e.g., `.pack()` and `.place()`) or consider using the `.grid()` method, which provides a grid-based layout system.

It's important to note that using `.pack()` and `.place()` together in the same container can lead to unpredictable results. Choose the method that best fits your layout requirements and use it consistently within the container.

In Python's Tkinter library, the `.grid()` method is used for organizing widgets in a grid-based layout. It provides a way to arrange widgets in rows and columns, allowing you to create more complex and structured GUI layouts.

The `.grid()` method uses a system of rows and columns to position widgets. Each widget is placed in a specific cell of the grid, defined by its row and column coordinates. You can also specify additional parameters like column span, row span, and alignment.

Here's an example that demonstrates the usage of `.grid()`:

```python
import tkinter as tk

root = tk.Tk()

label1 = tk.Label(root, text="Label 1")
label2 = tk.Label(root, text="Label 2")
label3 = tk.Label(root, text="Label 3")

label1.grid(row=0, column=0)  # Placed in the first row, first column
label2.grid(row=0, column=1)  # Placed in the first row, second column
label3.grid(row=1, column=0, columnspan=2)  # Placed in the second row, spans two columns

root.mainloop()
```

In this example, we create three labels (`label1`, `label2`, and `label3`) and use `.grid()` to position them in a grid layout. The first label is placed in the first row and first column, the second label is placed in the first row and second column, and the third label is placed in the second row, spanning two columns.

The `.grid()` method provides several parameters that can be used to customize the widget placement:

- `row`: Specifies the row index of the grid cell.
- `column`: Specifies the column index of the grid cell.
- `rowspan`: Specifies the number of rows that the widget should span.
- `columnspan`: Specifies the number of columns that the widget should span.
- `sticky`: Specifies the alignment of the widget within its cell (e.g., "N", "S", "E", "W", "NW", "NE", "SW", "SE").
- `padx`: Specifies horizontal padding around the widget.
- `pady`: Specifies vertical padding around the widget.

The `.grid()` method provides a flexible and powerful way to create more complex layouts compared to `.pack()` and `.place()`. By using rows and columns, you can easily align widgets in a grid-like fashion, accommodating different sizes and arrangements.

Note that you can use a combination of `.pack()`, `.place()`, and `.grid()` methods within the same container to achieve the desired layout. However, it's important to be cautious when mixing different geometry managers to avoid conflicts or unexpected results.

Explained by [ChatGPT](https://chat.openai.com/)

---

At the End of the day we build a converter to covert miles to kilometers

[Miles To Kilometers 🧮🏃](./MilesToKilometer.py)

Here is the code :

```python
from tkinter import *

FONT = ("Nohemi", 16, "bold")

def miles_to_kilometers():
    miles = float(miles_input.get())
    km = miles * 1.609
    kilometer_result_label.config(text=f"{km}")

window = Tk()
window.title("Miles To Kilometers 🧮🏃")
window.config(padx=20, pady=20)
window.minsize(width=400, height=100)

miles_input = Entry(width=7, font=FONT)
miles_input.grid(column=1, row=0)

miles_label = Label(text="Miles", font=FONT)
miles_label.grid(column=2, row=0)

is_equal = Label(text="is equal to", font=FONT)
is_equal.grid(column=0, row=1)

kilometer_result_label = Label(text="0", font=FONT)
kilometer_result_label.grid(column=1, row=1)

kilomete_label = Label(text="Km", font=FONT)
kilomete_label.grid(column=2, row=1)

calcutate_button = Button(text="Calculate", font=FONT, command=miles_to_kilometers)
calcutate_button.grid(column=2, row=0)

window.mainloop()
```
