from tkinter import *
from tkinter import messagebox
import pyperclip

BG_COLOR = "#d4dee7"
FG_COLOR = "#1b213c"
BASE_COLOR = "#20c6e9"
ACCENT_COLOR = "#e7a357"
FONT_NAME = 'Nohemi'


# --------------------------------------- PASSWORD GENERATOR ---------------------------------------
import random

def generate():
    characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    hash = ""
    for _ in range(8):
        hash += random.choice(characters)
    website = website_entry.get()
    password = f"{website}@{hash}"
    password_entry.insert(0, password)

    pyperclip.copy(password)
    messagebox.showinfo(title="Copied", message="Password copied to clipboard!")

# --------------------------------------- SAVE PASSWORD ---------------------------------------

def save():
    website = website_entry.get()
    username= username_entry.get()
    password = password_entry.get()
    
    if len(website) == 0 or len(username) == 0 or len(password) == 0:
       messagebox.showwarning(title="Empty Fields", message="You've left some blank fields!") 
    
    else:
        isOK = messagebox.askokcancel(title=website, message=f"These are the details entered: \n\n Email: {username} \n Password: {password} \n\n Do you want to save?")
        messagebox.showinfo(title="Success", message=" Your new password has been saved successfully!")
        if isOK:
            with open("passwords.txt", "a") as file:
                file.write(f"Website:{website} | Username/Email:{username} | Password:{password} \n")

                website_entry.delete(0, END)
                username_entry.delete(0, END)
                password_entry.delete(0, END)

# --------------------------------------- UI SETUP ---------------------------------------

window = Tk()
window.title("Password Manager 🔐")
window.config(padx=20, pady=20, bg=BG_COLOR)
window.minsize(width=940, height=540)

title = Label(text="Password Manager", fg=FG_COLOR, bg=BG_COLOR , font=(FONT_NAME, 35, "bold"))
title.grid(column=1, row=1, columnspan=3)

canvas = Canvas(width=300, height=300, bg=BG_COLOR, highlightthickness=0)
png = PhotoImage(file="lock.png")
canvas.create_image(150,150, image=png)
canvas.grid(column=1, row=2, columnspan=3)

# Website
website_label = Label(text="Website: ", fg=FG_COLOR, bg=BG_COLOR , font=(FONT_NAME, 15, "bold"))
website_label.grid(column=0, row=3)

website_entry = Entry(width=60, font=(FONT_NAME))
website_entry.focus()
website_entry.grid(column=1, row=3, columnspan=2)

# Username/Email
username_label = Label(text="Username: ", fg=FG_COLOR, bg=BG_COLOR , font=(FONT_NAME, 15, "bold"))
username_label.grid(column=0, row=4)

username_entry = Entry(width=60, font=(FONT_NAME))
username_entry.insert(0, "pruthivithejan.code@gmail.com" )
username_entry.grid(column=1, row=4, columnspan=2)

# Password
password_label = Label(text="Password: ", fg=FG_COLOR, bg=BG_COLOR , font=(FONT_NAME, 15, "bold"))
password_label.grid(column=0, row=5)

password_entry = Entry(width=35, font=(FONT_NAME))
password_entry.grid(column=1, row=5)

generate_button = Button(text="Generate Password", fg=FG_COLOR, bg=BASE_COLOR , font=(FONT_NAME, 15, "bold"), width=20, command=generate)
generate_button.grid(column=2, row=5)

# Add
add_button = Button(text="Add", fg=FG_COLOR, bg=ACCENT_COLOR , font=(FONT_NAME, 15, "bold"), width=40, command=save)
add_button.grid(column=1, row=6, columnspan=2, pady=10)


window.mainloop()