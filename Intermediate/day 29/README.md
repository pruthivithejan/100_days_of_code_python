# Day 29

## Today We're building a Password Manager 🔐

- [Password Manager 🔐](./PasswordManager.py)

<img src="./PasswordManager-Demo.png" alt="Password Manager - Demo">

---

## Copy To Clipboard

Here's an example of how you can use the `pyperclip` library in Python to copy text to the clipboard:

```python
import pyperclip

text_to_copy = "Hello, world!"

# Copy the text to the clipboard
pyperclip.copy(text_to_copy)

# Verify that the text has been copied by pasting it somewhere
print("Text copied to clipboard:", pyperclip.paste())
```

In this example, we import the `pyperclip` module and assign the text we want to copy to the `text_to_copy` variable. We then use `pyperclip.copy()` to copy the text to the clipboard. Finally, we use `pyperclip.paste()` to verify that the text has been successfully copied by printing it to the console.

Make sure you have the `pyperclip` library installed in your Python environment before running this code. You can install it using pip:

```
pip install pyperclip
```

Remember that the availability of clipboard functionality can depend on the operating system you are using.

Explained by [ChatGPT](https://chat.openai.com/)
