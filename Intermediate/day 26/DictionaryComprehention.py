# # ================================DICTIONARY COMPREHENTION=================================

# #  Example 1: Assign marks to students
# import random
# names = ['Alex', 'John', 'David', 'Frank', 'Evan', 'Thomas']

# student_scores = {student: random.randint(1, 100) for student in names}
# print(student_scores)

# #  Example 1: Checking passed students

# passed_students = {student: mark for (
#     student, mark) in student_scores.items() if mark > 50}
# print(passed_students)

# # Example 3: Number of characters in a sentence
# sentence = "What is Airspeed Velocity of an Unladen Swallow?"
# results = {word: len(word) for word in sentence.split()}
# print(results)

# Example 4: Celcius to Farenheight
weather_c = {
    "Monday": 12,
    "Tuesday": 14,
    "Wednesday": 15,
    "Thursday": 14,
    "Friday": 21,
    "Saturday": 22,
    "Sunday": 24,
}

weather_f = {day: int(temp)*9/5+32 for (day, temp) in weather_c.items()}
print(weather_f)
