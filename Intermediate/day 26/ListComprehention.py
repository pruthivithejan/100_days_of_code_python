# # ================================LIST COMPREHENTION=================================
# numbers = [1, 2, 3, 4, 5]

# # Example 1: Squaring each number
# squared_numbers = [x**2 for x in numbers]
# print(squared_numbers)  # Output: [1, 4, 9, 16, 25]

# # Example 2: Filtering even numbers (Conditional List Comprehention)
# even_numbers = [x for x in numbers if x % 2 == 0]
# print(even_numbers)  # Output: [2, 4]

# # Example 3: Filtering odd numbers (Conditional List Comprehention)
# odd_numbers = [x for x in numbers if x % 2 == 1]
# print(odd_numbers)  # Output: [2, 4]

# # =======TEST 4=======
# list = [1, 2, 3]
# new_list = [n+1 for n in list]
# print(new_list)

# =======TEST 5 (Conditional List Comprehention)=======
name = "Pruthivi Thejan"
letters_list = [letter for letter in name if letter != " "]
print(letters_list)
