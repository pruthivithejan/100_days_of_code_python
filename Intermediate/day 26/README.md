# Day 26

## List comprehension

List comprehension is a concise way to create lists in Python. It provides a compact syntax for generating a new list by iterating over an existing sequence or iterable object and applying an expression or condition. It combines the process of creating a list and iterating over it into a single line of code.

The general syntax of a list comprehension is as follows:

```python
[expression for item in iterable if condition]
```

Let's break down the different components of a list comprehension:

- Expression: This represents the transformation or calculation that is applied to each item in the iterable. It can be any valid Python expression that produces a value. The result of this expression becomes an element of the new list.

- Item: It refers to the individual elements or items from the iterable that are being processed. For each item in the iterable, the expression is applied to produce a value.

- Iterable: This is a sequence, collection, or any object that can be iterated over. It can be a list, tuple, string, dictionary, or any other iterable object.

- Condition (optional): It is an optional part of a list comprehension that allows filtering the items based on a specified condition. Only the items that satisfy the condition will be included in the new list. The condition is written after the "for" statement and is usually expressed as a Boolean expression.

Here's an example to illustrate the usage of list comprehension:

```python
numbers = [1, 2, 3, 4, 5]

# Example 1: Squaring each number
squared_numbers = [x**2 for x in numbers]
print(squared_numbers)  # Output: [1, 4, 9, 16, 25]

# Example 2: Filtering even numbers
even_numbers = [x for x in numbers if x % 2 == 0]
print(even_numbers)  # Output: [2, 4]
```

In Example 1, the expression x\*\*2 squares each number in the numbers list, resulting in a new list squared_numbers with the squared values.

In Example 2, the list comprehension filters the even numbers from the numbers list by using the condition x % 2 == 0. Only the numbers that satisfy this condition (i.e., divisible by 2) are included in the even_numbers list.

List comprehensions are concise, readable, and often more efficient than traditional for loops for creating new lists. However, complex or nested list comprehensions can become less readable, so it's important to balance readability and conciseness when using them.

- [Examples for List Comprehension](./ListComprehention.py)

---

## Dictionary comprehension

Dictionary comprehension is a concise way to create dictionaries in Python. It allows you to generate a new dictionary by iterating over an existing sequence or iterable object and specifying key-value pairs using an expression or condition. Similar to list comprehensions, dictionary comprehensions provide a compact syntax for creating dictionaries in a single line of code.

The general syntax of a dictionary comprehension is as follows:

```python
{key_expression: value_expression for item in iterable if condition}
```

Let's break down the different components of a dictionary comprehension:

- Key Expression: This represents the expression that generates the keys for the new dictionary. It can be any valid Python expression that produces a value.

- Value Expression: This represents the expression that generates the values for the new dictionary. It can also be any valid Python expression that produces a value.

- Item: It refers to the individual elements or items from the iterable that are being processed. For each item in the iterable, the key-value expressions are applied to generate the corresponding key-value pairs.

- Iterable: This is a sequence, collection, or any object that can be iterated over. It can be a list, tuple, string, dictionary, or any other iterable object.

- Condition (optional): It is an optional part of a dictionary comprehension that allows filtering the items based on a specified condition. Only the items that satisfy the condition will have their key-value pairs included in the new dictionary. The condition is written after the "for" statement and is usually expressed as a Boolean expression.

Here's an example to illustrate the usage of dictionary comprehension:

```python
numbers = [1, 2, 3, 4, 5]

# Example: Squaring numbers and creating a dictionary
squared_dict = {x: x**2 for x in numbers}
print(squared_dict)  # Output: {1: 1, 2: 4, 3: 9, 4: 16, 5: 25}
```

In the example above, the dictionary comprehension generates a new dictionary called squared_dict. For each number x in the numbers list, the key-value pair x: x\*\*2 is created, where the key is the number itself, and the value is the square of the number.

Dictionary comprehensions are a powerful and concise way to create dictionaries based on existing sequences or iterables. They are particularly useful when you want to transform or filter data to create a dictionary with specific key-value pairs.

- [Examples for Dictionary Comprehension](./DictionaryComprehention.py)

### Explained by [ChatGPT](https://chat.openai.com/)

---

This example shows how to use Pandas Library for looping through a data frame

```python
import pandas

student_dict = {
    "student": ["Harry", "James", "Lily"],
    "score": [56, 76, 98]
}


student_data_frame = pandas.DataFrame(student_dict)
# print(student_data_frame)

# Pandas Looping Method
for (index, row) in student_data_frame.iterrows():
    if row.score > 75:
        print(row.student)

```

---

And Finally, we build the NATO Alphabet program

- [NATO Alphabet 🅰️](./NATO%20Alphabet/main.py)
