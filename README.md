# 100 Days of Code - Python

### This is a Course that I geneuinely liked and used to make the Habit of coding in Python. I already know that this course itself will not make me a Python expert, but drive me enough to get there.

## 🚶 Journey

I kept tracking my progress of the 100 days of the course and commited them to this git repository. This is not 100% complete. Sometimes I haven't even watched the solutions to the challenges and left the code I wrote that worked just fine.

#### Beginner :

- I didn't track my progress in the first week, so code starts with day 8
- I tried to do all the challenges myself. Some I got right but written lot more lines than the solution.
- Sometimes I got stuck and followed along the solution.

#### Intermediate :

- From Day 16 I started taking notes inside the folder using a README.md file.
- I haven't included every basic concept of python in this repository. I've learned them in school and if you want to study them I highly recommend to visit [w3schools.com/python](https://www.w3schools.com/python/)
- I used LLMs to explain the concepts and tryied paying more attention to using them.
- I realized that you can't do this daily without breaking a day with your personal workflow. So always remember to come back or you'll break the cycle.
- Sometimes when I'm tired I didn't checked the solution if mine came closer to the final result.
- I took a long break after university started and now I have some extra time in the second semester. I'm back!

## 👍 Highly Recommended

100 Days of Code: The Complete Python Pro Boot camp for 2023 by Angela Yu.
Course Link (Not Sponsored) : https://www.udemy.com/course/100-days-of-code/

## 👨‍💻 Developed By

Pruthivi Thejan  
[@pruthivithejan](https://twitter.com/pruthivithejan)

## 💬 Contact

Send me an email if you need to contact me to discuss anything.  
Email - <pruthivithejan.code@gmail.com>
